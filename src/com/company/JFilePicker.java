package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by ThomasVisser on 04-01-17.
 */
public class JFilePicker extends JPanel {
    private String textFieldLabel;
    private String chooseButtonLabel;
    public String filePath;

    private JTextField textField;
    public JButton fileButton;
    private JLabel pickerLabel;
    private JFileChooser fileChooser;

    public void setButtonStatus(boolean status){this.fileButton.setEnabled(status); revalidate();}
    public void setTextFieldStatus(boolean status){this.textField.setEnabled(status); revalidate();}

    public JFilePicker() {
        this.textFieldLabel = "Pick a File:";
        this.chooseButtonLabel = "Choose File";

        fileChooser = new JFileChooser();

        setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        // creates the GUI
        pickerLabel = new JLabel(textFieldLabel);

        textField = new JTextField(10);
        textField.setEditable(false);
        textField.setForeground(Color.lightGray);

        fileButton = new JButton();
        fileButton.setText(chooseButtonLabel);

        fileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                filePath = buttonActionPerformed(evt);
            }
        });

        add(pickerLabel);
        add(textField);
        add(fileButton);
    }

    private String buttonActionPerformed(ActionEvent evt) {
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            String filePath = fileChooser.getSelectedFile().getAbsolutePath();
            textField.setText(filePath);
            return filePath;
        }
        return null;
    }
}
