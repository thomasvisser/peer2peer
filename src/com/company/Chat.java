package com.company;
import javax.swing.*;
import java.net.*;
import java.io.*;

/**
 * Created by ThomasVisser on 28-12-16.
 */
public class Chat extends Thread{
    private String state;
    private String username;
    private String ipAddress;
    private int portNumber;
    private boolean readFile = false;
    private String filename = "";
    private String peerUsername = "";
    private String filepath;
    public boolean connected = false;
    private JTextArea outputPanel;
    public boolean connectionMade = false;
    public boolean hostDisconnect = false;
    private boolean fileSendOverSocket = false;

    public Socket connectSock = null;
    private ServerSocket receiveSock = null;
    private DataInputStream in = null;
    private BufferedInputStream inBuf = null;

    public String getPeerUsername(){ return this.peerUsername;}

    public Chat(String state, String username, String ip, int portNumber, JTextArea outputPanel){
        this.state = state;
        this.username = username;
        this.ipAddress = ip;
        this.portNumber = portNumber;
        this.outputPanel = outputPanel;

        this.filepath = System.getProperty("user.home") + File.separator + "Downloads";
    }

    public void run(){

        if(state.equals("wait")) {
            outputPanel.append("Waiting for peers...\n");
        }
        sendAndReceive(this.ipAddress, this.portNumber, this.state);
    }

    public void sendAndReceive(String ip, int port, String state){

        //Create Socket and ServerSocket to communicate
        try {
            if(state.equals("connect")) {
                this.connectSock = new Socket(ip, port);
                this.receiveSock = new ServerSocket(port + 1);
            }else if(state.equals("wait")){
                this.receiveSock = new ServerSocket(port);
            }

        }catch (IOException ex) {
            System.out.println("ERROR " + ex);

            String message = "Peer is not available. \nTry again or connect with another peer";
            HandleActions.getInstance().showDialog(message);
            HandleActions.getInstance().updateData(true);
            HandleActions.getInstance().updateUI(false);
        }

        try {
            Socket peer = receiveSock.accept();
            this.connectionMade = true;
            this.in = new DataInputStream(peer.getInputStream());
            this.inBuf = new BufferedInputStream(in);

            StringBuilder line = new StringBuilder();

            if(state.equals("wait")) {
                String remoteIP = peer.getInetAddress().getHostAddress();
                this.connectSock = new Socket(remoteIP, port + 1);
            }

            this.connected = true;
            System.out.println(this.connected);

            while(true){
                byte[] file = null;

                while(true) {

                    if(!this.readFile) {
                        while (this.inBuf.available() > 0) {
                            int c = this.inBuf.read();
                            line.append((char) c);
                        }

                        if (line.length() > 0) break;
                    }
                    else if(this.readFile) {

                        int totalBytes = 0;

                        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
                        if(this.inBuf.available() > 0){

                            System.out.println("Bytes available " + this.inBuf.available());

                            while(this.inBuf.available() > 0) {

                                int c = this.inBuf.read();
                                byteArray.write(c);
                                totalBytes += 1;
                            }

                            System.out.println("byteArray size " + byteArray.size());

                            file = byteArray.toByteArray();
                            System.out.println("Total bytes read " + totalBytes);
                            break;
                        }
                    }
                }

                String message = line.toString();
                System.out.println("message: " + message); System.out.println("length: " + message.length());

                if(message.length() >= 9) {
                    if (message.substring(0, 9).equals("username:")) {
                        String username = message.substring(10, message.length());
                        this.peerUsername = username;

                        this.outputPanel.append("Connected with: " + "\"" + this.peerUsername + "\"\n");
                        line.setLength(0);
                        continue;
                    }

                    if(message.length() >= 14) {
                        if (message.substring(0, 14).equals("File incoming:")) {

                            this.readFile = true;
                            this.filename = message.substring(15, message.length());

                            this.filepath += File.separator;
                            this.filepath += this.filename;

                            System.out.println("Final FILEPATH: " + this.filepath);
                            line.setLength(0);
                            continue;
                        }
                    }

                    if(message.substring(0, 10).equals("disconnect")){

                        System.out.println("About to disconnect..");
                        if (!hostDisconnect) {
                            sendMessages send = new sendMessages(this.connectSock, this.username, "disconnect", this.outputPanel);
                            send.start();
                        }

                        HandleActions conn = HandleActions.getInstance();
                        conn.disconnect();
                        break;
                    }
                }

                //Write file to Downloads folder of machine if there is a file available
                if(this.readFile){

                    //Display File warning
                    FileDialog wrongInput = new FileDialog();
                    wrongInput.pack();
                    wrongInput.setVisible(true);

                    while(!wrongInput.buttonClicked){}

                    if(wrongInput.willAccept()) {
                        File thisFile = new File(this.filepath);
                        FileOutputStream fileOutput = new FileOutputStream(thisFile);
                        fileOutput.write(file);

                        fileOutput.close();

                        this.outputPanel.append(peerUsername + " send you a file!! Go check it out in Downloads\n\n");

                    }
                    else {
                        this.outputPanel.append("You rejected to download a file from " + this.peerUsername + "\n");

                    }

                    this.readFile = false;
                    this.fileSendOverSocket = true; System.out.println("fileSendOverSocket: " + this.fileSendOverSocket);
                    line.setLength(0);
                }
                else {

                    if(!this.fileSendOverSocket) {

                        if(message.length() >= 10) {
                            if (message.substring(0, 10).equals("disconnect")){ continue;}
                        }

                        this.outputPanel.append("\n" + this.peerUsername + " says: " + message + "\n\n");
                        line.setLength(0);
                    }
                    this.fileSendOverSocket = false; line.setLength(0);
                }
            }

        }catch(IOException ex) {
            System.out.println("ERROR " + ex);
        }
        finally {
            try {

                if(this.connectSock != null)
                    this.connectSock.close();
                if(this.receiveSock != null)
                    this.receiveSock.close();
                if(this.inBuf != null)
                    this.inBuf.close();
                if(this.in != null)
                    this.in.close();

                System.out.println("Streams and connections closed!!");
            }catch (IOException ex){
                System.out.println("ERROR " + ex);
            }
        }
    }
}
