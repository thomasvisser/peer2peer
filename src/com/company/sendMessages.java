package com.company;
import javax.swing.*;
import java.io.*;
import java.net.*;
/**
 * Created by ThomasVisser on 28-12-16.
 */
public class sendMessages extends Thread{

    private String username;
    private Socket sock;
    private DataOutputStream rawOut = null;
    private BufferedOutputStream bufOut = null;
    private boolean sendUsername;
    private String messageToSend = null;
    private JTextArea outputPanel;

    public sendMessages(Socket socket, String username, String message, JTextArea outputPanel){
        this.sock = socket;
        this.username = username;
        this.messageToSend = message;
        this.sendUsername = false;
        this.outputPanel = outputPanel;

        try {
            rawOut = new DataOutputStream(this.sock.getOutputStream());
            bufOut = new BufferedOutputStream(rawOut);
        }catch(IOException ex){
            System.out.println("ERROR " + ex);
        }
    }

    public sendMessages(Socket socket, String username){
        this.sock = socket;
        this.username = username;
        this.sendUsername = true;

        try {
            rawOut = new DataOutputStream(this.sock.getOutputStream());
            bufOut = new BufferedOutputStream(rawOut);
        }catch(IOException ex){
            System.out.println("ERROR " + ex);
        }
    }

    public void run(){

        try{
            if(sendUsername){
                bufOut.write(("username: " + username + "\r\n").getBytes());
                bufOut.flush();
                return;
            }

            bufOut.write((messageToSend + "\r\n").getBytes());
            bufOut.flush();

            if(!messageToSend.equals("disconnect"))
                outputPanel.append("You said: " + messageToSend + "\n");

        }catch(IOException ex){
            System.out.println("ERROR " + ex);
        }
    }
}
