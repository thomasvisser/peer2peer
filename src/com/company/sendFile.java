package com.company;
import java.io.*;
import java.net.*;
/**
 * Created by ThomasVisser on 28-12-16.
 */
public class sendFile extends Thread{

    private Socket sock;
    private DataOutputStream rawOut = null;
    private BufferedOutputStream bufOut = null;
    private String filePath;

    public sendFile(Socket socket, String filePath){
        this.sock = socket;
        this.filePath = filePath;

        try {
            rawOut = new DataOutputStream(this.sock.getOutputStream());
            bufOut = new BufferedOutputStream(rawOut);
        }catch(IOException ex){
            System.out.println("ERROR " + ex);
        }
    }

    public void run(){

        try{
            String splitFilePath[] = filePath.split("/");
            String fileName = splitFilePath[splitFilePath.length - 1];

            File fileToSend = new File(filePath);
            FileInputStream fileIn = new FileInputStream(fileToSend);

            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

            int bRead;
            int total = 0;
            while((bRead = fileIn.read()) != -1) {

                byteBuffer.write(bRead);
                total += 1;
            }

            System.out.println("Total bytes read: " + total);

            fileIn.close();

            bufOut.write(("File incoming: " + fileName).getBytes());
            bufOut.flush();

            try{
                Thread.sleep(200);
            }
            catch(Exception ex){
                System.out.println("ERROR " + ex);
            }

            byte[] buffer = byteBuffer.toByteArray();
            System.out.println("buffer size " + buffer.length);
            bufOut.write(buffer);
            bufOut.flush();
        }catch(IOException ex){
            System.out.println("ERROR " + ex);
        }
    }
}
