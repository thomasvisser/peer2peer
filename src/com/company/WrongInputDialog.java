package com.company;

import javax.swing.*;
import javax.tools.Tool;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WrongInputDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel messageLabel;

    public WrongInputDialog(String message) {
        this.messageLabel.setText(message);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screensize.width - getWidth()) / 2;
        int y = (screensize.height - getHeight()) / 2;
        setLocation(x - 100, y - 100);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });
    }

    private void onOK() {
        dispose();
    }
}
