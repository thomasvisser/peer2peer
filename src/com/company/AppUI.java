package com.company;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by ThomasVisser on 04-01-17.
 */
public class AppUI extends JFrame {
    private final static AppUI instance = new AppUI();

    //UI Components
    private JPanel mainPanel;
    private JTextField ipTextField;
    private JTextField portTextField;
    private JCheckBox connectToPeerCheckBox;
    private JTextField usernameTextField;
    private JButton connectButton;
    private JButton disconnectButton;
    private JButton sendButton;
    private JButton clearButton;
    private JTextArea inputTextArea;
    private JTextArea outputPanel;
    private JButton sendFileButton;
    private JLabel ipLabel;
    private JFilePicker filePicker;
    private JScrollPane scrollPane;
    private JLabel creditLabel;

    public static AppUI getInstance() {
        return instance;
    }

    //Getters for UI Components
    public JTextField getIPTextField() {
        return ipTextField;
    }
    public JTextField getPortTextField() {
        return portTextField;
    }
    public JCheckBox getConnectCheckBox() {
        return connectToPeerCheckBox;
    }
    public JTextField getUsernameTextField() {
        return usernameTextField;
    }
    public JButton getConnectButton() {
        return connectButton;
    }
    public JButton getDisconnectButton() {
        return disconnectButton;
    }
    public JButton getSendButton() {
        return sendButton;
    }
    public JButton getClearButton() {
        return clearButton;
    }
    public JButton getFileButton() {
        return sendFileButton;
    }
    public JTextArea getInputTextArea() {
        return inputTextArea;
    }
    public JTextArea getOutputPanel() {
        return outputPanel;
    }
    public JLabel getIpLabel() {
        return ipLabel;
    }
    public JFilePicker getFilePicker() {
        return filePicker;
    }

    private AppUI() {

        //Set some settings for the frame
        setContentPane(this.mainPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setResizable(false);
        setSize(610, 400);
        this.mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        //Set some settings for the objects
        Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
        this.inputTextArea.setBorder(border);
        this.outputPanel.setBorder(border);
        this.outputPanel.setEditable(false);
        this.outputPanel.setLineWrap(true);
        this.ipLabel.setText("Your IP address");

        this.inputTextArea.setEnabled(false);
        this.sendButton.setEnabled(false);
        this.disconnectButton.setEnabled(false);
        this.clearButton.setEnabled(false);

        //Set some settings for the scrollbar
        this.scrollPane.setVisible(true);
        this.scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        //Set some settings for the custom filePicker
        this.filePicker.setButtonStatus(false);
        this.filePicker.setTextFieldStatus(false);
        this.filePicker.setVisible(true);

        this.sendFileButton.setEnabled(false);

        //Set the position for the Frame on screen
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screenSize.width - getWidth()) / 2;
        int y = (screenSize.height - getHeight()) / 2;
        setLocation(x , y );

        //make the form visible
        setVisible(true);
    }

    public void addActionListeners(){

        //Add ActionListeners for active components
        HandleActions connection = HandleActions.getInstance();
        this.connectButton.addActionListener(connection);
        this.sendButton.addActionListener(connection);
        this.clearButton.addActionListener(connection);
        this.disconnectButton.addActionListener(connection);
        this.connectToPeerCheckBox.addActionListener(connection);
        this.sendFileButton.addActionListener(connection);
    }
}


