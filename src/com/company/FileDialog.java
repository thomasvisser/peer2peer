package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FileDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel labelText;
    public boolean buttonClicked = false;

    private boolean acceptFile = false;

    public boolean willAccept(){ return acceptFile;}

    public FileDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        this.labelText.setText("The Peer wants to send you file. Proceed?");

        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screensize.width - getWidth()) / 2;
        int y = (screensize.height - getHeight()) / 2;
        setLocation(x - 100, y - 100);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        this.acceptFile = true; buttonClicked = true;
        dispose();
    }

    private void onCancel() {
        this.acceptFile = false; buttonClicked = true;
        dispose();
    }
}
