package com.company;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

/**
 * Created by ThomasVisser on 07-01-17.
 */
public class HandleActions extends Thread implements ActionListener {

    private AppUI app = AppUI.getInstance();
    private final static HandleActions instance = new HandleActions();

    private String state;
    private String ipAddress;
    private String port;
    private String username;
    private boolean shouldConnect = false;
    private Chat chat;

    public static HandleActions getInstance(){
        return instance;
    }

    private HandleActions(){}

    @Override
    public void actionPerformed(ActionEvent e){

        switch (e.getActionCommand()){
            case "Connect!":
                updateData(false);

                boolean validInput = checkInput();
                if(!validInput) break;

                establishConnection(); break;
            case "Send!":
                sendMessage(false); break;
            case "Send File":
                sendFile(); break;
            case "Clear":
                this.app.getInputTextArea().setText(""); break;
            case "Connect to Peer":
                this.shouldConnect = this.app.getConnectCheckBox().isSelected();
                if(this.shouldConnect){ this.app.getIpLabel().setText("Peer's IP Address"); this.app.revalidate();}
                else this.app.getIpLabel().setText("Your IP Address"); this.app.revalidate(); break;
            case "Disconnect":
                sendMessage(true); break;
        }
    }

    public void updateData(boolean reset){

        if(!reset) {
            this.username = this.app.getUsernameTextField().getText();
            this.ipAddress = this.app.getIPTextField().getText();
            this.port = this.app.getPortTextField().getText();
            this.shouldConnect = this.app.getConnectCheckBox().isSelected();

            if (shouldConnect) {
                this.state = "connect";
            } else {
                this.state = "wait";
            }
        }else{
            this.username = "";
            this.port = "";
            this.ipAddress = "";
            this.shouldConnect = false;
            this.state = "";
        }

        //debug
        /*
        System.out.println("username " + this.username);
        System.out.println("ipAddress " + this.ipAddress);
        System.out.println("port " + this.port);
        System.out.println("shouldConnect " + this.shouldConnect);
        System.out.println("State: " + state);*/
    }

    private void establishConnection(){

        updateUI(true);

        this.chat = new Chat(this.state, this.username, this.ipAddress, Integer.parseInt(this.port), this.app.getOutputPanel());
        this.chat.start();

        this.start();
    }

    @Override
    public void run(){
        long startTime = System.nanoTime();
        long nanoSeconds;
        System.out.println("Start mSeconds: " + startTime);

        if(this.state != "wait") {
            while (true) {
                if (this.chat.connectionMade) {
                    System.out.println("Connection made!");
                    break;
                }
                System.out.println("mSeconds during: " + System.nanoTime());
                nanoSeconds = System.nanoTime() - startTime;

                System.out.println("mSeconds: " + nanoSeconds);
                if (nanoSeconds > 2000000000) {
                    String message = "Peer is not available. \nTry again or connect with another peer";
                    showDialog(message);
                    updateData(true);
                    updateUI(false);
                    return;
                }
            }
        }

        sendUsername sendUser = new sendUsername(this.chat, this.username);
        sendUser.sendIt();
    }

    private boolean checkInput(){
        String ultimateRegex = "^([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\." +
                "([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";

        String regexPort = "^\\d+$";

        System.out.println("IP " + this.ipAddress);
        System.out.println("Port " + this.port);

        boolean isIP = Pattern.matches(ultimateRegex, this.ipAddress);
        boolean isPort = Pattern.matches(regexPort, this.port);

        System.out.println("valid IP " + isIP);
        System.out.println("valid port " + isPort);

        if(!isIP) {
            String message = "Please enter a valid IP address";
            showDialog(message);
            return false;
        }
        else if(!isPort) {
            String message = "Please enter a valid port";
            showDialog(message);
            return false;
        }

        if(this.username.equals("")){
            String message = "Please enter a username";
            showDialog(message);
            return false;
        }

        return true;
    }

    public void updateUI(boolean disableCom){

        String LipAddress;
        String Lport;
        String Lusername;
        Color color;

        if(disableCom) {
            LipAddress = this.ipAddress;
            Lport = this.port;
            Lusername = this.username;
            this.app.getConnectCheckBox().setSelected(this.shouldConnect);
            color = Color.lightGray;
            this.app.getConnectCheckBox().setEnabled(false);
        }
        else{
            LipAddress = ""; Lport = ""; Lusername = ""; this.state = "";
            this.app.getConnectCheckBox().setSelected(false);
            this.app.getConnectCheckBox().setEnabled(true);
            color = Color.black;
            this.app.getOutputPanel().setText("");
        }

        this.app.getIPTextField().setText(LipAddress); this.app.getIPTextField().setEditable(!disableCom); this.app.getIPTextField().setForeground(color);
        this.app.getPortTextField().setText(Lport); this.app.getPortTextField().setEditable(!disableCom); this.app.getPortTextField().setForeground(color);
        this.app.getUsernameTextField().setText(Lusername); this.app.getUsernameTextField().setEditable(!disableCom); this.app.getUsernameTextField().setForeground(color);

        this.app.getInputTextArea().setEnabled(disableCom);
        this.app.getSendButton().setEnabled(disableCom);
        this.app.getClearButton().setEnabled(disableCom);
        this.app.getFileButton().setEnabled(disableCom);
        this.app.getDisconnectButton().setEnabled(disableCom);
        this.app.getConnectButton().setEnabled(!disableCom);
        this.app.getFilePicker().setButtonStatus(disableCom);
        this.app.getFilePicker().setTextFieldStatus(disableCom);
    }

    private void sendMessage(boolean disconnect){

        String message;

        if(disconnect){
            message = "disconnect";
            this.chat.hostDisconnect = true;
        }
        else{
            message = this.app.getInputTextArea().getText();
        }

        if(message.equals("")){
            String dialogMessage = "Please enter a message to send";
            showDialog(dialogMessage);
            return;
        }else if(!this.chat.connectionMade){
            String dialogMessage = "Please connect with a peer first";
            showDialog(dialogMessage);
            return;
        }

        sendMessages sendIt = new sendMessages(this.chat.connectSock, this.username, message, this.app.getOutputPanel());
        sendIt.start();

        this.app.getInputTextArea().setText("");

       //if(disconnect)
         //   disconnect();
    }

    private void sendFile(){

        String path = this.app.getFilePicker().filePath;

        System.out.println("FilePath: " + path);

        if(path.equals("") || path == null){
            String message = "Please choose a file first";
            showDialog(message);
            return;
        }else if(!this.chat.connectionMade){
            String dialogMessage = "Please connect with a peer first";
            showDialog(dialogMessage);
            return;
        }

        sendFile sendIt = new sendFile(this.chat.connectSock, path);
        sendIt.start();
    }

    public void showDialog(String message){
        WrongInputDialog wrongInput = new WrongInputDialog(message);
        wrongInput.pack();
        wrongInput.setVisible(true);
    }

    public void disconnect(){

        updateUI(false);
        updateData(true);

        this.shouldConnect = false;
        this.chat.hostDisconnect = false;

        String disconnectMessage = "Connection with " + this.chat.getPeerUsername() + "closed";
        showDialog(disconnectMessage);

        System.out.println("Chat: " + this.chat);
    }
}
